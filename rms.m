function rmsVal = rms(input)
rmsVal = sqrt(dot(input,input)/numel(input));
end